<?php

use App\Http\Controllers\Admin87Controller;
use App\Http\Controllers\Login87Controller;
use App\Http\Controllers\Users87Controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/signup87');
});

Route::group(['middleware' => ['initakLogged']], function () {
    // Login & Register
    Route::view('/signup87', 'register');
    Route::view('/87signin', 'login');
    Route::post('/signup87', [Login87Controller::class, 'registerHandler87']);
    Route::post('/87signin', [Login87Controller::class, 'loginHandler87']);
});

Route::group(['middleware' => ['iniAdmin']], function () {
    //dashboard && detail user
    Route::get('/dashboard87', [Admin87Controller::class, 'admindashboardpaged87']);
    Route::get('/detail87/{id}', [Admin87Controller::class, 'detailAdminPage87']);

    // update user
    Route::get('/update87/user/{id}/status', [Admin87Controller::class, 'updateStatusUser87']);
    Route::post('/update87/user/{id}/agama', [Admin87Controller::class, 'updateAdminAgamaUser87']);

    // CRUD AGAMA
    // Show all agama
    Route::get("/agama87", [Admin87Controller::class, "adminagamapage87"]);
    // add agama
    Route::post("/agama87", [Admin87Controller::class, "admincreateAdmin87"]);
    // show edit agama & update agama
    Route::get("/agama87/{id}/edit", [Admin87Controller::class, 'editAdminAgamaPage87']);
    Route::post("/agama87/{id}/update", [Admin87Controller::class, 'ubahAdminAgama87']);
    // delete agama
    Route::get("/agama87/{id}/delete", [Admin87Controller::class, 'hapusAdminAgama87']);
});

Route::group(['middleware' => ['iniUser']], function () {
    // dashboard user
    Route::get('/profile87', [Users87Controller::class, 'halamanProfil87']);
    Route::get('/welcome87', [Users87Controller::class, 'welcome']);
    //change Password
    Route::get('/changePassword87', [Users87Controller::class, 'editPasswordPage87']);
    Route::post('/updatePassword87', [Users87Controller::class, 'ubahUserPassword87']);

    // edit profile user
    Route::post('/perbaruiProfil87', [Users87Controller::class, 'updateProfil87']);
    Route::post('/unggahPicProfil87', [Users87Controller::class, 'unggahFotoProfil87']);
    Route::post('/unggahPicKTP87', [Users87Controller::class, 'unggahFotoKTP87']);
});

Route::get('/logout87', [Login87Controller::class, 'logoutHandler87'])->middleware('iniLogged');
